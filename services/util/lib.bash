#!/bin/bash

function yes_or_no {
	YES_NO="[y/n]"
	case $2 in
		[Yy]) 
			YES_NO="[Y/n]"
			DEFAULT=0 ;;
		[Nn])
			YES_NO="[N/y]"
			DEFAULT=1 ;;
	esac

	while true; do
		read -p "$1 $YES_NO: " yn
		case $yn in
			[Yy]) return 0 ;;
			[Nn]) return 1 ;;
			"")
				if [ ! -z "$DEFAULT" ]; then
					return $DEFAULT
				fi
				echo "Resposta inválida. Responda com Y ou N."
				;;
			*) echo "Resposta inválida. Responda com Y ou N." ;;
		esac
	done
}

function download-file {
	wget -O - "$1"
}

function check_permissions {
	if [ "$(id -u)" != "0" ]; then
		return 1
	fi
	return 0
}

function assert_permissions {
	MESSAGE="${MESSAGE}"
	MESSAGE="${MESSAGE:=O script deve ser rodado como sudo}"
	if ! check_permissions ; then
		echo "$MESSAGE" 1>&2
		exit 1
	fi
}